
$(function(){


    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 600);
                return false;
            }
        }
    });



    var currentIndex = 0,
        items = $('#slideshow').find('div'),
        itemAmt = items.length;

    function cycleItems() {
        var item = items.eq(currentIndex);
        var time = 300;
        items.fadeOut(time);
        setTimeout(function(){
            item.fadeIn(time);
            setTimeout(function(){
                $('#nextButton').attr("disabled", false);
                $('#prevButton').attr("disabled", false);
            }, 100);
        },time);


    }

    function firstItem(){
        var item = items.eq(currentIndex);
        items.hide();
        item.show();
    }

    firstItem();



    $('#nextButton').click(function() {

        $('#nextButton').attr("disabled", true);

        currentIndex += 1;
        if (currentIndex > itemAmt - 1) {
            currentIndex = 0;
        }
        cycleItems();


    });

    $('#prevButton').click(function() {

        $('#prevButton').attr("disabled", true);
        currentIndex -= 1;
        if (currentIndex < 0) {
            currentIndex = itemAmt - 1;
        }
        cycleItems();
    });
    
    $('#sendButton').click(function() {
    
    	var name = $("#theName").val();
    	var message = $("#theMessage").val();
    	var body = message + "\n\nRegards, \n" + name;

    	window.location.href('mailto:tkaczykmichal94@gmail.com?subject=question from page&body=' + body);
    });
    
    


});
